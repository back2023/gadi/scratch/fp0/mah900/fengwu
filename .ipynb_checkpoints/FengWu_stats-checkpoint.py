import xarray as xr
import numpy  as np
import os
from   pathlib import Path
import time
from timeit import default_timer as timer
from datetime import timedelta
import FengWu_util
import multiprocessing
import pprint 
import logging
import json
#import ujson
logging.basicConfig(level=logging.DEBUG)


def stat_series (output_data_dir, input_datetime, output_stat_dir, steps):
    sfc_vars = ['u10', 'v10', 't2m', 'msl']
    pl_vars  = ['z', 'q', 'u', 'v', 't']
    levels   = [50, 100, 150, 200, 250, 300, 400, 500, 600, 700, 850, 925, 1000]

    common_config_tuple = (output_data_dir, input_datetime, output_stat_dir, steps)
    args = []
    for i in range (len(sfc_vars)):
        args.append ( (sfc_vars[i], None, i) +  common_config_tuple )  
    #for i in range ( len(pl_vars) ):   
    #    for j in range ( len(levels) ):
    #        args.append ( (pl_vars[i], levels[j], len(sfc_vars) + len(levels) * i + j ) + common_config_tuple )    
    
    jobs = []
    with multiprocessing.Pool(12) as pool:
        return_dict_list = pool.starmap(stat_var_level_series, args )
    return_dict = {}
    for item in return_dict_list:    
    #    print(a, "\n")
        return_dict[ list(item.keys())[0] ] =  item[ list(item.keys())[0] ]
    #return_dict = { item.keys():item.values() for item in return_dict_list}
    logging.info( os.path.join ( output_stat_dir, input_datetime ) )
    #with open( os.path.join (output_stat_dir, input_datetime), 'w') as file:
    #    file.write( ujson.dumps(return_dict) )
    json.dump( return_dict, open( os.path.join ( output_stat_dir, input_datetime ), 'w' ) )


def stat_var_level_series (era5_var, level, pred_var_index, output_data_dir, input_datetime, output_stat_dir, steps ): #, return_dict):
    #logging.info( f"{era5_var} {pred_var_index} {level}")
    start = timer()
    #levels   = [50, 100, 150, 200, 250, 300, 400, 500, 600, 700, 850, 925, 1000]    
    stats_dict = {}
    stats_dict["input_datetime"] = input_datetime
    stats_dict["era5_var"] = era5_var
    stats_dict["pred_var_index"] = pred_var_index       
    stats_dict["level"]  = level
    stats_dict["output_datetime"] = {}   
    
    cache_year_ref  = 0
    cache_month_ref = 0
    era5_cache_data = ""
    output_datetime = FengWu_util.six_hour_ahead ( input_datetime )
    for i in range( int(steps) ):         
        #print (output_datetime)            
        if  cache_year_ref != int (output_datetime[0:4]) or cache_month_ref != int (output_datetime[5:7]):
            logging.info (f"Cache change {era5_var}, {level}: From ({cache_year_ref}, {cache_month_ref}) to ( {int(output_datetime[0:4])}, {int(output_datetime[5:7])} ) ")
            
            cache_month_datetime = FengWu_util.get_all_datetimes_within_a_month (output_datetime) 
            if level:
                pl_era5_path    = FengWu_util.get_pl_path (era5_var, output_datetime) 
                era5_cache_data = xr.open_dataset(pl_era5_path)[[era5_var]].sel(time=cache_month_datetime, level=level) #.load()
            else:
                sfc_era5_path   = FengWu_util.get_sfc_path (era5_var, output_datetime)
                era5_cache_data = xr.open_dataset(sfc_era5_path)[[era5_var]].sel(time=cache_month_datetime)  #.load()
            era5_cache_data = era5_cache_data.roll(longitude=int(1440/2)) 
            cache_year_ref  = int (output_datetime[0:4])
            cache_month_ref = int (output_datetime[5:7])           

        date_list_for_acc = FengWu_util.n_datetimes (output_datetime, 7)
        #for j in range ( len(levels) ):
        era5_ground_truth_acc = era5_cache_data[era5_var].sel(time=date_list_for_acc) 
        era5_ground_truth     = era5_cache_data[era5_var].sel(time=[output_datetime]) 
        
        pred_data   =  np.load(os.path.join( output_data_dir, f"{input_datetime}_output-{i:03d}_{output_datetime}.npy"))[pred_var_index] 
        pred_xarray =  xr.DataArray(np.expand_dims(pred_data , axis=0), coords=era5_ground_truth.coords, dims=era5_ground_truth.dims)
        
        rmse = compute_weighted_rmse(pred_xarray , era5_ground_truth, mean_dims=xr.ALL_DIMS).to_numpy().item()
        mae  = compute_weighted_mae (pred_xarray , era5_ground_truth, mean_dims=xr.ALL_DIMS).to_numpy().item() 
        acc  = compute_weighted_acc (pred_xarray , era5_ground_truth_acc, mean_dims=xr.ALL_DIMS).to_numpy().item()
        #print (f"{rmse:10.4f}  {mae:10.4f}  {acc:10.4f}")
        stats_dict["output_datetime"][output_datetime] = {"rmse": rmse, "mae": mae, "acc": acc}        
        output_datetime = FengWu_util.six_hour_ahead ( output_datetime )

    end = timer()
    stats_dict["time"] = timedelta(seconds=end-start).total_seconds()
    return_dict = {}
    #return_dict[ (input_datetime, era5_var, level) ] = stats_dict
    return_dict[ f"{input_datetime}_{era5_var}_{level if level else 'None'}" ] = stats_dict
    
    return return_dict


"""

def sfc_var_stat_series (sfc_var, pred_var_index, output_data_dir, input_datetime, output_stat_dir, steps, return_dict):
    start = timer()
    stats_dict = {}
    stats_dict[input_datetime] = {}
    stats_dict[input_datetime][sfc_var] = {}
    stats_dict[input_datetime][sfc_var]["sfc_var"] = sfc_var
    stats_dict[input_datetime][sfc_var]["input_datetime"] = input_datetime
    stats_dict[input_datetime][sfc_var]["pred_var_index"]  = pred_var_index   
    stats_dict[input_datetime][sfc_var]["output_datetime"] = {}
    #print (sfc_var, pred_var_index)
    cache_year_ref  = 0
    cache_month_ref = 0
    sfc_cache_data  = ""
    output_datetime = FengWu_util.six_hour_ahead ( input_datetime )
    for i in range( int(steps) ):         
        #print (output_datetime)       
        sfc_path         = FengWu_util.get_sfc_path (sfc_var, output_datetime) 

        if  cache_year_ref != int (output_datetime[0:4]) or cache_month_ref != int (output_datetime[5:7]):
            cache_datetime = FengWu_util.get_all_datetimes_within_a_month (output_datetime) 
            sfc_cache_data = xr.open_dataset(sfc_path)[[sfc_var]].sel(time=cache_datetime).roll(longitude=int(1440/2)) #.load()
            cache_year_ref  = int (output_datetime[0:4])
            cache_month_ref = int (output_datetime[5:7])
            print ("Cache changed")

        date_list_for_acc = FengWu_util.n_datetimes (output_datetime, 7)
        sfc_ground_truth_acc  = sfc_cache_data[sfc_var].sel(time=date_list_for_acc) 
        sfc_ground_truth      = sfc_cache_data[sfc_var].sel(time=[output_datetime]) 
        
        sfc_pred_data   = np.load(os.path.join( output_data_dir, f"{input_datetime}_output-{i:03d}_{output_datetime}.npy"))[pred_var_index] 
        sfc_pred_xarray =  xr.DataArray(np.expand_dims(sfc_pred_data , axis=0), coords=sfc_ground_truth.coords, dims=sfc_ground_truth.dims)
        
        rmse = compute_weighted_rmse(sfc_pred_xarray , sfc_ground_truth, mean_dims=xr.ALL_DIMS).to_numpy().item()
        mae  = compute_weighted_mae (sfc_pred_xarray , sfc_ground_truth, mean_dims=xr.ALL_DIMS).to_numpy().item() 
        acc  = compute_weighted_acc (sfc_pred_xarray , sfc_ground_truth_acc, mean_dims=xr.ALL_DIMS).to_numpy().item()
        #print (f"{rmse:10.4f}  {mae:10.4f}  {acc:10.4f}")
        stats_dict[input_datetime][sfc_var]["output_datetime"][output_datetime] = {}
        stats_dict[input_datetime][sfc_var]["output_datetime"][output_datetime]["rmse"] = rmse
        stats_dict[input_datetime][sfc_var]["output_datetime"][output_datetime]["mae"]  = mae
        stats_dict[input_datetime][sfc_var]["output_datetime"][output_datetime]["acc"]  = acc
        #break
        #print ()           
        output_datetime = FengWu_util.six_hour_ahead ( output_datetime )

    end = timer()
    #print(timedelta(seconds=end-start))
    stats_dict[input_datetime]["time"] = timedelta(seconds=end-start)
    #return_dict[sfc_var] = {sfc_var: stats_dict }
    return_dict[sfc_var]  = stats_dict



def pl_var_level_stat_series (pl_var, level, pred_var_index, output_data_dir, input_datetime, output_stat_dir, steps, return_dict):
    start = timer()

    #levels   = [50, 100, 150, 200, 250, 300, 400, 500, 600, 700, 850, 925, 1000]
    
    #stats_dict[input_datetime] = {}
    #stats_dict[ (input_datetime, pl_var, level) ] = {}
    #stats_dict[ (input_datetime, pl_var, level) ]["input_datetime"] = input_datetime
    #stats_dict[ (input_datetime, pl_var, level) ]["pl_var"] = pl_var
    #stats_dict[ (input_datetime, pl_var, level) ]["pred_var_index"] = pred_var_index       
    #stats_dict[ (input_datetime, pl_var, level) ]["level"]  = level
    #stats_dict[ (input_datetime, pl_var, level) ]["output_datetime"] = {}
    stats_dict = {}
    stats_dict["input_datetime"] = input_datetime
    stats_dict["pl_var"] = pl_var
    stats_dict["pred_var_index"] = pred_var_index       
    stats_dict["level"]  = level
    stats_dict["output_datetime"] = {}
    
    #print (sfc_var, pred_var_index)
    cache_year_ref  = 0
    cache_month_ref = 0
    pl_cache_level_data   = ""
    output_datetime = FengWu_util.six_hour_ahead ( input_datetime )
    for i in range( int(steps) ):         
        #print (output_datetime)       
        pl_path = FengWu_util.get_pl_path (pl_var, output_datetime) 

        if  cache_year_ref != int (output_datetime[0:4]) or cache_month_ref != int (output_datetime[5:7]):
            cache_datetime = FengWu_util.get_all_datetimes_within_a_month (output_datetime) 
            pl_cache_level_data  = xr.open_dataset(pl_path)[[pl_var]].sel(time=cache_datetime, level=level).roll(longitude=int(1440/2)) #.load()
            cache_year_ref  = int (output_datetime[0:4])
            cache_month_ref = int (output_datetime[5:7])
            print ("Cache changed")

        date_list_for_acc = FengWu_util.n_datetimes (output_datetime, 7)
        #for j in range ( len(levels) ):
        pl_ground_truth_acc = pl_cache_level_data[pl_var].sel(time=date_list_for_acc) 
        pl_ground_truth     = pl_cache_level_data[pl_var].sel(time=[output_datetime]) 
        
        pl_pred_data   =  np.load(os.path.join( output_data_dir, f"{input_datetime}_output-{i:03d}_{output_datetime}.npy"))[pred_var_index] 
        pl_pred_xarray =  xr.DataArray(np.expand_dims(pl_pred_data , axis=0), coords=pl_ground_truth.coords, dims=pl_ground_truth.dims)
        
        rmse = compute_weighted_rmse(pl_pred_xarray , pl_ground_truth, mean_dims=xr.ALL_DIMS).to_numpy().item()
        mae  = compute_weighted_mae (pl_pred_xarray , pl_ground_truth, mean_dims=xr.ALL_DIMS).to_numpy().item() 
        acc  = compute_weighted_acc (pl_pred_xarray , pl_ground_truth_acc, mean_dims=xr.ALL_DIMS).to_numpy().item()
        #print (f"{rmse:10.4f}  {mae:10.4f}  {acc:10.4f}")
        #stats_dict[ (input_datetime, pl_var, level) ]["output_datetime"][output_datetime] = {}
        #stats_dict[ (input_datetime, pl_var, level) ]["output_datetime"][output_datetime]["rmse"] = rmse
        #stats_dict[ (input_datetime, pl_var, level) ]["output_datetime"][output_datetime]["mae"]  = mae
        #stats_dict[ (input_datetime, pl_var, level) ]["output_datetime"][output_datetime]["acc"]  = acc
        stats_dict["output_datetime"][output_datetime] = {}
        stats_dict["output_datetime"][output_datetime]["rmse"] = rmse
        stats_dict["output_datetime"][output_datetime]["mae"]  = mae
        stats_dict["output_datetime"][output_datetime]["acc"]  = acc
        
        #break
        #print ()           
        output_datetime = FengWu_util.six_hour_ahead ( output_datetime )

    end = timer()
    #print(timedelta(seconds=end-start))
    #stats_dict[ (input_datetime, pl_var, level) ]["time"] = timedelta(seconds=end-start)
    stats_dict["time"] = timedelta(seconds=end-start)
    #return_dict[pl_var] = {pl_var: stats_dict }
    return_dict[ (input_datetime, pl_var, level) ]  = stats_dict

"""
"""

def pl_var_stat_series (pl_var, pred_var_index, output_data_dir, input_datetime, output_stat_dir, steps, return_dict):
    start = timer()

    levels   = [50, 100, 150, 200, 250, 300, 400, 500, 600, 700, 850, 925, 1000]
    stats_dict = {}
    stats_dict[input_datetime] = {}
    stats_dict[input_datetime][pl_var] = {}
    stats_dict[input_datetime][pl_var]["pl_var"] = pl_var
    stats_dict[input_datetime][pl_var]["input_datetime"] = input_datetime
    stats_dict[input_datetime][pl_var]["pred_var_index"]  = pred_var_index   
    stats_dict[input_datetime][pl_var]["output_datetime"] = {}
    #stats_dict[input_datetime][pl_var]["output_datetime"]["levels"] = {}
    #print (sfc_var, pred_var_index)
    cache_year_ref   = 0
    cache_month_ref  = 0
    pl_cache_data  = ""
    output_datetime = FengWu_util.six_hour_ahead ( input_datetime )
    for i in range( int(steps) ):         
        #print (output_datetime)       
        pl_path = FengWu_util.get_pl_path (pl_var, output_datetime) 

        if  cache_year_ref != int (output_datetime[0:4]) or cache_month_ref != int (output_datetime[5:7]):
            cache_datetime = FengWu_util.get_all_datetimes_within_a_month (output_datetime) 
            pl_cache_data  = xr.open_dataset(pl_path)[[pl_var]].sel(time=cache_datetime, level=levels).roll(longitude=int(1440/2)) #.load()
            cache_year_ref  = int (output_datetime[0:4])
            cache_month_ref = int (output_datetime[5:7])
            print ("Cache changed")

        date_list_for_acc = FengWu_util.n_datetimes (output_datetime, 7)

        for j in range ( len(levels) ):
            pl_ground_truth_acc  = pl_cache_data[pl_var].sel(time=date_list_for_acc, level = levels[j]) 
            pl_ground_truth      = pl_cache_data[pl_var].sel(time=[output_datetime], level = levels[j]) 
            
            pl_pred_data   =  np.load(os.path.join( output_data_dir, f"{input_datetime}_output-{i:03d}_{output_datetime}.npy"))[pred_var_index + j] 
            pl_pred_xarray =  xr.DataArray(np.expand_dims(pl_pred_data , axis=0), coords=pl_ground_truth.coords, dims=pl_ground_truth.dims)
            
            rmse = compute_weighted_rmse(pl_pred_xarray , pl_ground_truth, mean_dims=xr.ALL_DIMS).to_numpy().item()
            mae  = compute_weighted_mae (pl_pred_xarray , pl_ground_truth, mean_dims=xr.ALL_DIMS).to_numpy().item() 
            acc  = compute_weighted_acc (pl_pred_xarray , pl_ground_truth_acc, mean_dims=xr.ALL_DIMS).to_numpy().item()
            #print (f"{rmse:10.4f}  {mae:10.4f}  {acc:10.4f}")
            stats_dict[input_datetime][pl_var]["output_datetime"][output_datetime] = {}
            stats_dict[input_datetime][pl_var]["output_datetime"][output_datetime]["levels"] = {}
            stats_dict[input_datetime][pl_var]["output_datetime"][output_datetime]["levels"][levels[j]] = {}
            stats_dict[input_datetime][pl_var]["output_datetime"][output_datetime]["levels"][levels[j]]["rmse"] = rmse
            stats_dict[input_datetime][pl_var]["output_datetime"][output_datetime]["levels"][levels[j]]["mae"]  = mae
            stats_dict[input_datetime][pl_var]["output_datetime"][output_datetime]["levels"][levels[j]]["acc"]  = acc
        #break
        #print ()           
        output_datetime = FengWu_util.six_hour_ahead ( output_datetime )

    end = timer()
    #print(timedelta(seconds=end-start))
    stats_dict[input_datetime]["time"] = timedelta(seconds=end-start)
    #return_dict[pl_var] = {pl_var: stats_dict }
    return_dict[pl_var]  = stats_dict

"""
"""

def stat_compare (output_data_dir, input_datetime, output_stat_dir, steps):
    sfc_vars = ['u10', 'v10', 't2m', 'msl']
    pl_vars  = ['z', 'q', 'u', 'v', 't']
    levels   = [50, 100, 150, 200, 250, 300, 400, 500, 600, 700, 850, 925, 1000]
    start = timer()

    cache_year  = 0
    cache_month = 0
    sfc_cache_data  = ""
    output_datetime = six_hour_ahead ( input_datetime )
    for i in range( int(steps) ):         
        print (output_datetime)

        
        #date_list_for_acc = [output_datetime]
        #tmp_datetime = output_datetime
        #for _ in range ((28*4)):         
        #    date_list_for_acc.append (six_hour_ahead (tmp_datetime  ))
        #    tmp_datetime = six_hour_ahead (tmp_datetime)
        #date_list_for_acc = all_datetimes_within_a_month (output_datetime)
        #date_list_for_acc = three_datetimes (output_datetime)
        date_list_for_acc = n_datetimes (output_datetime, 7)
        #print (date_list_for_acc)     
        
        pred_data = np.load(os.path.join( output_data_dir, f"{input_datetime}_output-{i:03d}_{output_datetime}.npy")) 
        #print (pred_data.shape) 
        #print (pred_data.shape[0])            
        #for j in range(pred_data.shape[0]):
        #    pass
        for j in range (len (sfc_vars )):
            print (j, sfc_vars[j])
            sfc_pred         = pred_data[j]
            sfc_path         = get_sfc_path (sfc_vars[j], output_datetime) 

            if  cache_year != int (output_datetime[0:4]) or cache_month != int (output_datetime[5:7]):
                cache_datetime = get_all_datetimes_within_a_month (output_datetime) 
                sfc_cache_data = xr.open_dataset(sfc_path)[[sfc_vars[j]]].sel(time=cache_datetime).roll(longitude=int(1440/2)) #.load()
                cache_year  = int (output_datetime[0:4])
                cache_month = int (output_datetime[5:7])
                print (sfc_cache_data)

            
            #sfc_ground_truth      = xr.open_dataset(sfc_path)[sfc_vars[j]].sel(time=[output_datetime]).roll(longitude=int(1440/2)).load()
            #sfc_ground_truth_acc  = xr.open_dataset(sfc_path)[sfc_vars[j]].sel(time=date_list_for_acc).roll(longitude=int(1440/2)).load()
            #sfc_ground_truth      = sfc_ground_truth_acc.sel(time=[output_datetime]) 
            sfc_ground_truth_acc  = sfc_cache_data[sfc_vars[j]].sel(time=date_list_for_acc) 
            sfc_ground_truth      = sfc_cache_data[sfc_vars[j]].sel(time=[output_datetime]) 

            #print (sfc_ground_truth_acc) 
            #print (sfc_ground_truth) 
            #print (sfc_ground_truth.latitude)  
            
            #print ( ( np.expand_dims(sfc_pred , axis=0) ).shape )
            sfc_pred_xarray =  xr.DataArray(np.expand_dims(sfc_pred , axis=0), coords=sfc_ground_truth.coords, dims=sfc_ground_truth.dims)
            #print (sfc_pred_xarray, "\n" )
            #sfc_ground_truth_nparray  =  sfc_ground_truth.to_array().squeeze().squeeze().to_numpy()
            #print (sfc_ground_truth_nparray.shape)
            #rmse = compute_weighted_rmse(sfc_pred, sfc_ground_truth_nparray, sfc_ground_truth.latitude, mean_dims=xr.ALL_DIMS)

            #print (sfc_pred)
            #print (sfc_pred_xarray)
            #print (sfc_pred_xarray.count(), "\n")
            #print (sfc_ground_truth.count(), "\n")
            
            rmse = compute_weighted_rmse(sfc_pred_xarray , sfc_ground_truth, mean_dims=xr.ALL_DIMS)
            mae  = compute_weighted_mae (sfc_pred_xarray , sfc_ground_truth, mean_dims=xr.ALL_DIMS)
            acc  = compute_weighted_acc (sfc_pred_xarray , sfc_ground_truth_acc, mean_dims=xr.ALL_DIMS)
            print (f"{rmse.to_numpy():10.4f}  {mae.to_numpy():10.4f}  {acc.to_numpy():10.4f}")

            break
        #break    
            
        for j in range (len(pl_vars)):
            break
            # print (pl_vars[j], j)
            for k in range (len(levels)):
                print( 4 + j * len(levels) + k,  pl_vars[j], levels [k])
                tmp = pred_data[ 4 + j * len(levels) + k ]
        print ()    
        
        output_datetime = six_hour_ahead ( output_datetime )

    end = timer()
    print(timedelta(seconds=end-start))
"""
    
############################################################################################
### Stats

def compute_weighted_rmse(da_fc, da_true, mean_dims=xr.ALL_DIMS):
    """
    Compute the RMSE with latitude weighting from two xr.DataArrays.

    Args:
        da_fc (xr.DataArray): Forecast. Time coordinate must be validation time.
        da_true (xr.DataArray): Truth.
        mean_dims: dimensions over which to average score
    Returns:
        rmse: Latitude weighted root mean squared error
    """
    error = da_fc - da_true
    weights_lat = np.cos(np.deg2rad(error.latitude))
    weights_lat /= weights_lat.mean()
    rmse = np.sqrt(((error)**2 * weights_lat).mean(mean_dims))
    return rmse
    

def compute_weighted_mae(da_fc, da_true, mean_dims=xr.ALL_DIMS):
    """
    Compute the MAE with latitude weighting from two xr.DataArrays.
    Args:
        da_fc (xr.DataArray): Forecast. Time coordinate must be validation time.
        da_true (xr.DataArray): Truth.
        mean_dims: dimensions over which to average score
    Returns:
        mae: Latitude weighted root mean absolute error
    """
    error = da_fc - da_true
    #weights_lat = np.cos(np.deg2rad(error.lat))
    weights_lat = np.cos(np.deg2rad(error.latitude))
    weights_lat /= weights_lat.mean()
    mae = (np.abs(error) * weights_lat).mean(mean_dims)
    return mae


def compute_weighted_acc(da_fc, da_true, mean_dims=xr.ALL_DIMS):
    """
    Compute the ACC with latitude weighting from two xr.DataArrays.
    WARNING: Does not work if datasets contain NaNs

    Args:
        da_fc (xr.DataArray): Forecast. Time coordinate must be validation time.
        da_true (xr.DataArray): Truth.
        mean_dims: dimensions over which to average score
    Returns:
        acc: Latitude weighted acc
    """

    clim = da_true.mean('time')
    #print ("clim:", clim)
    try:
        t = np.intersect1d(da_fc.time, da_true.time)
        fa = da_fc.sel(time=t) - clim
    except AttributeError:
        t = da_true.time.values
        fa = da_fc - clim
    a = da_true.sel(time=t) - clim
    #a =  - clim
    #print ("a:", a)

    #weights_lat = np.cos(np.deg2rad(da_fc.lat))
    weights_lat = np.cos(np.deg2rad(da_fc.latitude))
    weights_lat /= weights_lat.mean()
    w = weights_lat

    fa_prime = fa - fa.mean()
    a_prime = a - a.mean()
    #print ("fa_prime:", fa_prime)
    #print ("a_prime: ", a_prime)

    acc = (
            np.sum(w * fa_prime * a_prime) /
            np.sqrt(
                np.sum(w * fa_prime ** 2) * np.sum(w * a_prime ** 2)
            )
    )
    return acc

