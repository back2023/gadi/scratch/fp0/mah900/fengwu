import xarray as xr
import numpy  as np
import os
from   pathlib import Path
import time
from timeit import default_timer as timer
from datetime import timedelta

import onnx
import onnxruntime as ort
import FengWu_util

def data_make (datetime):

    sfc_vars = ['u10', 'v10', 't2m', 'msl']
    pl_vars  = ['z', 'q', 'u', 'v', 't']
    levels   = [50, 100, 150, 200, 250, 300, 400, 500, 600, 700, 850, 925, 1000]

    new_data = []
    
    for sfc_var in sfc_vars:
        sfc_path = FengWu_util.get_sfc_path (sfc_var, datetime) 
        sfc_data = xr.open_dataset(sfc_path)[[sfc_var]].sel(time=[datetime]).roll(longitude=int(1440/2)).load()
        #print (sfc_var)
        #new_data[sfc_var] = sfc_data.to_array().squeeze().squeeze().to_numpy()
        new_data.append(sfc_data.to_array().squeeze().squeeze().to_numpy())
        sfc_data.close()
        #print(new_data)

    for pl_var in pl_vars:
        pl_path = FengWu_util.get_pl_path (pl_var, datetime) 
        for level in levels :
            pl_data = xr.open_dataset( pl_path )[[pl_var]].sel(time=[datetime], level=[level]).roll(longitude=int(1440/2)).load()
            #print (pl_var + str(level))
            #new_data[pl_var + str(level)] = pl_data.to_array().squeeze().squeeze().squeeze().to_numpy()      
            new_data.append(pl_data.to_array().squeeze().squeeze().to_numpy())
            pl_data.close()
        #print(new_data)
    numpyArray = np.array( new_data ) 
    return numpyArray
        
    
def input_make (year, month, day, hour, save_dir):

    if FengWu_util.check_month_day_hour (year, month, day, hour) !=0 : 
        exit ("Exit: Invalid data/time")

    save_date_time = f"{year:04d}-{month:02d}-{day:02d}T{hour:02d}" 
    #print (save_date_time)
    save_date_dir = os.path.join( save_dir, save_date_time )
    print (save_date_dir)
    Path(save_date_dir).mkdir(parents=True, exist_ok=True)
    
    input2_datetime  =  FengWu_util.to_datetime(year, month, day, hour)
    print (input2_datetime)
    numpyArray2 = data_make (input2_datetime)
    save_path2 = os.path.join( save_date_dir, f"{save_date_time}_input2.npy" )
    print (save_path2)
    np.save( save_path2, numpyArray2 )

    input1_datetime = FengWu_util.to_datetime( *FengWu_util.six_hour_before_date(year, month, day, hour) )
    print ( input1_datetime )
    numpyArray1 = data_make (input1_datetime)
    save_path1  = os.path.join( save_date_dir, f"{save_date_time}_input1.npy" )
    print (save_path1)
    np.save( save_path1, numpyArray1 )

    return save_date_dir, save_date_time


def inference (model_dir, model_name, in_stats_dir, input_data_dir, input_datetime, output_data_dir, steps):

    # The directory of your input and output data
    #input_data_dir = './input_data'
    #output_data_dir = './output_data'
    model_path = os.path.join(model_dir, model_name)
    #model_6 = onnx.load('fengwu.onnx')
    model_6 = onnx.load(model_path)
    
    # Set the behavier of onnxruntime
    options = ort.SessionOptions()
    options.enable_cpu_mem_arena=False
    options.enable_mem_pattern = False
    options.enable_mem_reuse = False
    # Increase the number for faster inference and more memory consumption
    options.intra_op_num_threads = 1
    
    # Set the behavier of cuda provider
    cuda_provider_options = {'arena_extend_strategy':'kSameAsRequested',}
    
    # Initialize onnxruntime session for Pangu-Weather Models
    #ort_session_6 = ort.InferenceSession('fengwu.onnx', sess_options=options, providers=[('CUDAExecutionProvider', cuda_provider_options)])
    ort_session_6 = ort.InferenceSession(model_path, sess_options=options, providers=[('CUDAExecutionProvider', cuda_provider_options)])
    
    #data_mean = np.load("data_mean.npy")[:, np.newaxis, np.newaxis]
    #data_std = np.load("data_std.npy")[:, np.newaxis, np.newaxis]
    data_mean = np.load( os.path.join(in_stats_dir, "data_mean.npy"))[:, np.newaxis, np.newaxis]
    data_std  = np.load( os.path.join(in_stats_dir, "data_std.npy") )[:, np.newaxis, np.newaxis]
    
    
    #input1 = np.load(os.path.join(input_data_dir, 'input1.npy')).astype(np.float32)
    #input2 = np.load(os.path.join(input_data_dir, 'input2.npy')).astype(np.float32)
    input1 = np.load(os.path.join(input_data_dir, f"{input_datetime}_input1.npy" )).astype(np.float32)
    input2 = np.load(os.path.join(input_data_dir, f"{input_datetime}_input2.npy" )).astype(np.float32)
    #input1 = np.load(os.path.join( input_path1 )).astype(np.float32)
    #input2 = np.load(os.path.join( input_path2 )).astype(np.float32)                       
    # input1 = np.random.rand(69, 721, 1440)
    # input2 = np.random.rand(69, 721, 1440)
    
    input1_after_norm = (input1 - data_mean) / data_std
    input2_after_norm = (input2 - data_mean) / data_std
    input = np.concatenate((input1_after_norm, input2_after_norm), axis=0)[np.newaxis, :, :, :]
    input = input.astype(np.float32)

    output_datetime = FengWu_util.six_hour_ahead ( input_datetime )
    #for i in range(56):
    #start = time.time()
    start = timer()
    for i in range( int(steps) ):
        output = ort_session_6.run(None, {'input':input})[0]
        input = np.concatenate((input[:, 69:], output[:, :69]), axis=1)
        output = (output[0, :69] * data_std) + data_mean
        print (f"{output_datetime}   {output.shape}  ")
      # np.save(os.path.join(output_data_dir, f"output_{i}.npy"), output)       #保存输出
        np.save(os.path.join(output_data_dir, f"{input_datetime}_output-{i:03d}_{output_datetime}.npy"), output)     
        output_datetime = FengWu_util.six_hour_ahead ( output_datetime )
    #end = time.time()
    #print("Time:", end - start) 
    end = timer()
    print(timedelta(seconds=end-start))
     
    