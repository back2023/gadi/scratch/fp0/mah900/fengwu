import xarray as xr
import numpy  as np
import os
from   pathlib import Path
import time
from timeit import default_timer as timer
from datetime import timedelta


import os
import numpy as np
import onnx
import onnxruntime as ort


def n_datetimes (datetime, n):
    year  = int (datetime[0:4])
    month = int (datetime[5:7])
    day   = int (datetime[8:10])
    hour  = int (datetime[11:13])
    
    if get_month_days( year, month ) - day >= n:
        start_date = to_datetime(year, month, day, hour)
    else:
        start_date = to_datetime(year, month, day - (n - (get_month_days(year, month) - day)) , hour)
        #print (3 - (get_month_days(year, month) - day) , ", start_date:", start_date)
    tmp_datetime = start_date
    three_datetimes_list = [] 
    for _ in range ( n*4 ):
        three_datetimes_list.append (tmp_datetime)
        tmp_datetime = six_hour_ahead (tmp_datetime)
    return three_datetimes_list 


def three_datetimes (datetime):
    year  = int (datetime[0:4])
    month = int (datetime[5:7])
    day   = int (datetime[8:10])
    hour  = int (datetime[11:13])
    
    if get_month_days( year, month ) - day >= 3:
        start_date = to_datetime(year, month, day, hour)
    else:
        start_date = to_datetime(year, month, day - (3 - (get_month_days(year, month) - day)) , hour)
        #print (3 - (get_month_days(year, month) - day) , ", start_date:", start_date)
    tmp_datetime = start_date
    three_datetimes_list = [] 
    for _ in range ( 12 ):
        three_datetimes_list.append (tmp_datetime)
        tmp_datetime = six_hour_ahead (tmp_datetime)
    return three_datetimes_list 

def get_all_datetimes_within_a_month (datetime):
    year  = int (datetime[0:4])
    month = int (datetime[5:7])

    tmp_datetime = to_datetime(year, month, 1, 0)
    datetime_list = [] 
    for _ in range ( get_month_days(year, month )*4 ):
        datetime_list.append (tmp_datetime)
        tmp_datetime = six_hour_ahead (tmp_datetime)
    return datetime_list

def six_hour_ahead (datetime):
    try:
        year  = int (datetime[0:4])
        month = int (datetime[5:7])
        day   = int (datetime[8:10])
        hour  = int (datetime[11:13])
    except:
        exit ("Date/time is not valid")
    #print (year, month, day, hour)
    hour += 6
    if hour >= 24: # Carefull about time 2400 is actually 0000 next day
        hour = 0  
        day += 1             
    if day > get_month_days(year, month):
        day = 1
        month += 1
    if month > 12:
        month = 1 
        year += 1  
    return f"{year:04d}-{month:02d}-{day:02d}T{hour:02d}"     
    

def to_datetime(year, month, day, hour): 
    return f"{year:04d}-{month:02d}-{day:02d}T{hour:02d}:00:00.00" 

def six_hour_before_date(year, month, day, hour): 
    #print (year, month, day, hour)
    if hour >= 6:
        return year, month, day, hour-6
   
    hour = 24 - 6
    day -= 1
    if day <= 0:
        day = get_month_days(year, month)
        month -= 1
    if month <= 0:
        month = 12
        year -= 1
    #print (year, month, day, hour)
    return year, month, day, hour
   
def check_month_day_hour (year, month, day, hour):    
    if hour < 0 or hour > 24 or hour%6!=0:
        return -1
    if day <1 or day >  get_month_days(year, month):
        return -2      
    if month < 1 or month > 12:
        return -3
    return 0

def is_leap_year (year):
    if year % 100 == 0 and year % 400 == 0 :
        return 1
    elif year % 4 == 0 :
        return 1
    else:
        return 0      

def get_month_days(year, month):
    month_days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    leap = 0
    if month == 2:
        leap = is_leap_year (year)
    return (month_days[month-1]+leap)              
        
def get_file_suffix(year, month):
    month_days = get_month_days(year, month)          
    return  f"{year:04d}{month:02d}01-{year:04d}{month:02d}{month_days:02d}"    

def get_sfc_path(era5_var, datetime):
    year   = int (datetime[ :4])
    month  = int (datetime[5:7])  
    file_suffix = get_file_suffix( int(year), int (month) )

    if   era5_var == 't2m': file_var_name = '2t'
    elif era5_var == 'u10': file_var_name = '10u' 
    elif era5_var == 'v10': file_var_name = '10v'               
    elif era5_var == 'u100':file_var_name = '100u'
    elif era5_var == 'v100':file_var_name = '100v'
    else: file_var_name =  era5_var             
        
    sfc_path = "/g/data/rt52/era5/single-levels/reanalysis/" + \
                f"{file_var_name}/{year}/{file_var_name}_era5_oper_sfc_{file_suffix}.nc"                                     
    return sfc_path                                            

def get_pl_path(era5_var, datetime):
    year   = int (datetime[ :4])
    month  = int (datetime[5:7])  
    file_suffix = get_file_suffix( int(year), int (month) )
    
    pl_path = "/g/data/rt52/era5/pressure-levels/reanalysis/" + \
                f"{era5_var}/{year}/{era5_var}_era5_oper_pl_{file_suffix}.nc"
    return pl_path

def data_make (datetime):

    sfc_vars = ['u10', 'v10', 't2m', 'msl']
    pl_vars  = ['z', 'q', 'u', 'v', 't']
    levels   = [50, 100, 150, 200, 250, 300, 400, 500, 600, 700, 850, 925, 1000]

    new_data = []
    
    for sfc_var in sfc_vars:
        sfc_path = get_sfc_path (sfc_var, datetime) 
        sfc_data = xr.open_dataset(sfc_path)[[sfc_var]].sel(time=[datetime]).roll(longitude=int(1440/2)).load()
        #print (sfc_var)
        #new_data[sfc_var] = sfc_data.to_array().squeeze().squeeze().to_numpy()
        new_data.append(sfc_data.to_array().squeeze().squeeze().to_numpy())
        sfc_data.close()
        #print(new_data)

    for pl_var in pl_vars:
        pl_path = get_pl_path (pl_var, datetime) 
        for level in levels :
            pl_data = xr.open_dataset( pl_path )[[pl_var]].sel(time=[datetime], level=[level]).roll(longitude=int(1440/2)).load()
            #print (pl_var + str(level))
            #new_data[pl_var + str(level)] = pl_data.to_array().squeeze().squeeze().squeeze().to_numpy()      
            new_data.append(pl_data.to_array().squeeze().squeeze().to_numpy())
            pl_data.close()
        #print(new_data)
    numpyArray = np.array( new_data ) 
    return numpyArray
        
    
def input_make (year, month, day, hour, save_dir):

    if check_month_day_hour (year, month, day, hour) !=0 : 
        exit ("Exit: Invalid data/time")

    #save_date_time = f"{input2_datetime[:4]:04s}{input2_datetime[5:7]:02s}{input2_datetime[8:10]:02s}"  
    save_date_time = f"{year:04d}-{month:02d}-{day:02d}T{hour:02d}" 
    #print (save_date_time)
    #tmp_dir = os.path.join( os.environ.get("TMPDIR", "/tmp"), save_date_time )
    save_date_dir = os.path.join( save_dir, save_date_time )
    print (save_date_dir)
    Path(save_date_dir).mkdir(parents=True, exist_ok=True)
    
    #input1_datetime = f"{year:04d}-{month:02d}-{day:02d}T{hour:02d}:00:00.0000" 
    input2_datetime  =  to_datetime(year, month, day, hour)
    print (input2_datetime)
    #data2 = data_make (input2_datetime)
    #for a in new_data.keys():
    #    print (a)    
    #print (new_data)
    #numpyArray = np.array( list(new_data.values()) ) # np.array( list(new_data.items()) )
    #numpyArray2 = np.array( data2 ) 
    numpyArray2 = data_make (input2_datetime)
    #np.save('/tmp/temp.npy', numpyArray)
    save_path2 = os.path.join( save_date_dir, f"{save_date_time}_input2.npy" )
    #np.save('/tmp/temp.npy', numpyArray2)
    print (save_path2)
    np.save( save_path2, numpyArray2 )

    input1_datetime = to_datetime( *six_hour_before_date(year, month, day, hour) )
    print ( input1_datetime )
    #data1 = data_make (input1_datetime)
    #numpyArray1 = np.array( data1 ) 
    numpyArray1 = data_make (input1_datetime)
    save_path1  = os.path.join( save_date_dir, f"{save_date_time}_input1.npy" )
    print (save_path1)
    np.save( save_path1, numpyArray1 )

    #loaded_data2 = np.load(save_path2)
    #print (loaded_data2.shape)
    #loaded_data1 = np.load(save_path1)
    #print (loaded_data1.shape)

    return save_date_dir, save_date_time


def inference (model_dir, model_name, in_stats_dir, input_data_dir, input_datetime, output_data_dir, steps):

    # The directory of your input and output data
    #input_data_dir = './input_data'
    #output_data_dir = './output_data'
    model_path = os.path.join(model_dir, model_name)
    #model_6 = onnx.load('fengwu.onnx')
    model_6 = onnx.load(model_path)
    
    # Set the behavier of onnxruntime
    options = ort.SessionOptions()
    options.enable_cpu_mem_arena=False
    options.enable_mem_pattern = False
    options.enable_mem_reuse = False
    # Increase the number for faster inference and more memory consumption
    options.intra_op_num_threads = 1
    
    # Set the behavier of cuda provider
    cuda_provider_options = {'arena_extend_strategy':'kSameAsRequested',}
    
    # Initialize onnxruntime session for Pangu-Weather Models
    #ort_session_6 = ort.InferenceSession('fengwu.onnx', sess_options=options, providers=[('CUDAExecutionProvider', cuda_provider_options)])
    ort_session_6 = ort.InferenceSession(model_path, sess_options=options, providers=[('CUDAExecutionProvider', cuda_provider_options)])
    
    #data_mean = np.load("data_mean.npy")[:, np.newaxis, np.newaxis]
    #data_std = np.load("data_std.npy")[:, np.newaxis, np.newaxis]
    data_mean = np.load( os.path.join(in_stats_dir, "data_mean.npy"))[:, np.newaxis, np.newaxis]
    data_std  = np.load( os.path.join(in_stats_dir, "data_std.npy") )[:, np.newaxis, np.newaxis]
    
    
    #input1 = np.load(os.path.join(input_data_dir, 'input1.npy')).astype(np.float32)
    #input2 = np.load(os.path.join(input_data_dir, 'input2.npy')).astype(np.float32)
    input1 = np.load(os.path.join(input_data_dir, f"{input_datetime}_input1.npy" )).astype(np.float32)
    input2 = np.load(os.path.join(input_data_dir, f"{input_datetime}_input2.npy" )).astype(np.float32)
    #input1 = np.load(os.path.join( input_path1 )).astype(np.float32)
    #input2 = np.load(os.path.join( input_path2 )).astype(np.float32)                       
    # input1 = np.random.rand(69, 721, 1440)
    # input2 = np.random.rand(69, 721, 1440)
    
    input1_after_norm = (input1 - data_mean) / data_std
    input2_after_norm = (input2 - data_mean) / data_std
    input = np.concatenate((input1_after_norm, input2_after_norm), axis=0)[np.newaxis, :, :, :]
    input = input.astype(np.float32)

    output_datetime = six_hour_ahead ( input_datetime )
    #for i in range(56):
    #start = time.time()
    start = timer()
    for i in range( int(steps) ):
        output = ort_session_6.run(None, {'input':input})[0]
        input = np.concatenate((input[:, 69:], output[:, :69]), axis=1)
        output = (output[0, :69] * data_std) + data_mean
        print (f"{output_datetime}   {output.shape}  ")
      # np.save(os.path.join(output_data_dir, f"output_{i}.npy"), output)       #保存输出
        np.save(os.path.join(output_data_dir, f"{input_datetime}_output-{i:03d}_{output_datetime}.npy"), output)     
        output_datetime = six_hour_ahead ( output_datetime )
    #end = time.time()
    #print("Time:", end - start) 
    end = timer()
    print(timedelta(seconds=end-start))
     
        
def stat_compare (output_data_dir, input_datetime, output_stat_dir, steps):
    sfc_vars = ['u10', 'v10', 't2m', 'msl']
    pl_vars  = ['z', 'q', 'u', 'v', 't']
    levels   = [50, 100, 150, 200, 250, 300, 400, 500, 600, 700, 850, 925, 1000]
    start = timer()

    cache_year  = 0
    cache_month = 0
    sfc_cache_data  = ""
    output_datetime = six_hour_ahead ( input_datetime )
    for i in range( int(steps) ):         
        print (output_datetime)

        
        #date_list_for_acc = [output_datetime]
        #tmp_datetime = output_datetime
        #for _ in range ((28*4)):         
        #    date_list_for_acc.append (six_hour_ahead (tmp_datetime  ))
        #    tmp_datetime = six_hour_ahead (tmp_datetime)
        #date_list_for_acc = all_datetimes_within_a_month (output_datetime)
        #date_list_for_acc = three_datetimes (output_datetime)
        date_list_for_acc = n_datetimes (output_datetime, 7)
        #print (date_list_for_acc)     
        
        pred_data = np.load(os.path.join( output_data_dir, f"{input_datetime}_output-{i:03d}_{output_datetime}.npy")) 
        #print (pred_data.shape) 
        #print (pred_data.shape[0])            
        #for j in range(pred_data.shape[0]):
        #    pass
        for j in range (len (sfc_vars )):
            print (j, sfc_vars[j])
            sfc_pred         = pred_data[j]
            sfc_path         = get_sfc_path (sfc_vars[j], output_datetime) 

            if  cache_year != int (output_datetime[0:4]) or cache_month != int (output_datetime[5:7]):
                cache_datetime = get_all_datetimes_within_a_month (output_datetime) 
                sfc_cache_data = xr.open_dataset(sfc_path)[[sfc_vars[j]]].sel(time=cache_datetime).roll(longitude=int(1440/2)) #.load()
                cache_year  = int (output_datetime[0:4])
                cache_month = int (output_datetime[5:7])
                print (sfc_cache_data)

            
            #sfc_ground_truth      = xr.open_dataset(sfc_path)[sfc_vars[j]].sel(time=[output_datetime]).roll(longitude=int(1440/2)).load()
            #sfc_ground_truth_acc  = xr.open_dataset(sfc_path)[sfc_vars[j]].sel(time=date_list_for_acc).roll(longitude=int(1440/2)).load()
            #sfc_ground_truth      = sfc_ground_truth_acc.sel(time=[output_datetime]) 
            sfc_ground_truth_acc  = sfc_cache_data[sfc_vars[j]].sel(time=date_list_for_acc) 
            sfc_ground_truth      = sfc_cache_data[sfc_vars[j]].sel(time=[output_datetime]) 

            #print (sfc_ground_truth_acc) 
            #print (sfc_ground_truth) 
            #print (sfc_ground_truth.latitude)  
            
            #print ( ( np.expand_dims(sfc_pred , axis=0) ).shape )
            sfc_pred_xarray =  xr.DataArray(np.expand_dims(sfc_pred , axis=0), coords=sfc_ground_truth.coords, dims=sfc_ground_truth.dims)
            #print (sfc_pred_xarray, "\n" )
            #sfc_ground_truth_nparray  =  sfc_ground_truth.to_array().squeeze().squeeze().to_numpy()
            #print (sfc_ground_truth_nparray.shape)
            #rmse = compute_weighted_rmse(sfc_pred, sfc_ground_truth_nparray, sfc_ground_truth.latitude, mean_dims=xr.ALL_DIMS)

            #print (sfc_pred)
            #print (sfc_pred_xarray)
            #print (sfc_pred_xarray.count(), "\n")
            #print (sfc_ground_truth.count(), "\n")
            
            rmse = compute_weighted_rmse(sfc_pred_xarray , sfc_ground_truth, mean_dims=xr.ALL_DIMS)
            mae  = compute_weighted_mae (sfc_pred_xarray , sfc_ground_truth, mean_dims=xr.ALL_DIMS)
            acc  = compute_weighted_acc (sfc_pred_xarray , sfc_ground_truth_acc, mean_dims=xr.ALL_DIMS)
            print (f"{rmse.to_numpy():10.4f}  {mae.to_numpy():10.4f}  {acc.to_numpy():10.4f}")

            break
        #break    
            
        for j in range (len(pl_vars)):
            break
            # print (pl_vars[j], j)
            for k in range (len(levels)):
                print( 4 + j * len(levels) + k,  pl_vars[j], levels [k])
                tmp = pred_data[ 4 + j * len(levels) + k ]
        print ()    
        
        output_datetime = six_hour_ahead ( output_datetime )

    end = timer()
    print(timedelta(seconds=end-start))
    
############################################################################################
### Stats

def compute_weighted_rmse(da_fc, da_true, mean_dims=xr.ALL_DIMS):
    """
    Compute the RMSE with latitude weighting from two xr.DataArrays.

    Args:
        da_fc (xr.DataArray): Forecast. Time coordinate must be validation time.
        da_true (xr.DataArray): Truth.
        mean_dims: dimensions over which to average score
    Returns:
        rmse: Latitude weighted root mean squared error
    """
    error = da_fc - da_true
    weights_lat = np.cos(np.deg2rad(error.latitude))
    weights_lat /= weights_lat.mean()
    rmse = np.sqrt(((error)**2 * weights_lat).mean(mean_dims))
    return rmse
    

def compute_weighted_mae(da_fc, da_true, mean_dims=xr.ALL_DIMS):
    """
    Compute the MAE with latitude weighting from two xr.DataArrays.
    Args:
        da_fc (xr.DataArray): Forecast. Time coordinate must be validation time.
        da_true (xr.DataArray): Truth.
        mean_dims: dimensions over which to average score
    Returns:
        mae: Latitude weighted root mean absolute error
    """
    error = da_fc - da_true
    #weights_lat = np.cos(np.deg2rad(error.lat))
    weights_lat = np.cos(np.deg2rad(error.latitude))
    weights_lat /= weights_lat.mean()
    mae = (np.abs(error) * weights_lat).mean(mean_dims)
    return mae


def compute_weighted_acc(da_fc, da_true, mean_dims=xr.ALL_DIMS):
    """
    Compute the ACC with latitude weighting from two xr.DataArrays.
    WARNING: Does not work if datasets contain NaNs

    Args:
        da_fc (xr.DataArray): Forecast. Time coordinate must be validation time.
        da_true (xr.DataArray): Truth.
        mean_dims: dimensions over which to average score
    Returns:
        acc: Latitude weighted acc
    """

    clim = da_true.mean('time')
    #print ("clim:", clim)
    try:
        t = np.intersect1d(da_fc.time, da_true.time)
        fa = da_fc.sel(time=t) - clim
    except AttributeError:
        t = da_true.time.values
        fa = da_fc - clim
    a = da_true.sel(time=t) - clim
    #a =  - clim
    #print ("a:", a)

    #weights_lat = np.cos(np.deg2rad(da_fc.lat))
    weights_lat = np.cos(np.deg2rad(da_fc.latitude))
    weights_lat /= weights_lat.mean()
    w = weights_lat

    fa_prime = fa - fa.mean()
    a_prime = a - a.mean()
    #print ("fa_prime:", fa_prime)
    #print ("a_prime: ", a_prime)

    acc = (
            np.sum(w * fa_prime * a_prime) /
            np.sqrt(
                np.sum(w * fa_prime ** 2) * np.sum(w * a_prime ** 2)
            )
    )
    return acc

