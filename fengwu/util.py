import xarray as xr
import numpy  as np
import os
from   pathlib import Path
import time
from timeit import default_timer as timer
from datetime import timedelta
import logging
import fengwu

def n_datetimes (datetime, n):
    year  = int (datetime[0:4])
    month = int (datetime[5:7])
    day   = int (datetime[8:10])
    hour  = int (datetime[11:13])
    
    if get_month_days( year, month ) - day >= n:
        start_date = to_datetime(year, month, day, hour)
    else:
        start_date = to_datetime(year, month, day - (n - (get_month_days(year, month) - day)) , hour)
        #print (3 - (get_month_days(year, month) - day) , ", start_date:", start_date)
    tmp_datetime = start_date
    three_datetimes_list = [] 
    for _ in range ( n*4 ):
        three_datetimes_list.append (tmp_datetime)
        tmp_datetime = six_hour_ahead (tmp_datetime)
    return three_datetimes_list 


def three_datetimes (datetime):
    year  = int (datetime[0:4])
    month = int (datetime[5:7])
    day   = int (datetime[8:10])
    hour  = int (datetime[11:13])
    
    if get_month_days( year, month ) - day >= 3:
        start_date = to_datetime(year, month, day, hour)
    else:
        start_date = to_datetime(year, month, day - (3 - (get_month_days(year, month) - day)) , hour)
        #print (3 - (get_month_days(year, month) - day) , ", start_date:", start_date)
    tmp_datetime = start_date
    three_datetimes_list = [] 
    for _ in range ( 12 ):
        three_datetimes_list.append (tmp_datetime)
        tmp_datetime = six_hour_ahead (tmp_datetime)
    return three_datetimes_list 

def get_all_datetimes_within_a_month (datetime):
    year  = int (datetime[0:4])
    month = int (datetime[5:7])

    tmp_datetime = to_datetime(year, month, 1, 0)
    datetime_list = [] 
    for _ in range ( get_month_days(year, month )*4 ):
        datetime_list.append (tmp_datetime)
        tmp_datetime = six_hour_ahead (tmp_datetime)
    return datetime_list

def six_hour_ahead (datetime):
    try:
        year  = int (datetime[0:4])
        month = int (datetime[5:7])
        day   = int (datetime[8:10])
        hour  = int (datetime[11:13])
    except:
        exit ("Date/time is not valid")
    #print (year, month, day, hour)
    hour += 6
    if hour >= 24: # Carefull about time 2400 is actually 0000 next day
        hour = 0  
        day += 1             
    if day > get_month_days(year, month):
        day = 1
        month += 1
    if month > 12:
        month = 1 
        year += 1  
    return f"{year:04d}-{month:02d}-{day:02d}T{hour:02d}"     
    

def to_datetime(year, month, day, hour): 
    return f"{year:04d}-{month:02d}-{day:02d}T{hour:02d}:00" #":00.00" 

def six_hour_before_date(year, month, day, hour): 
    #print (year, month, day, hour)
    if hour >= 6:
        return year, month, day, hour-6
   
    hour = 24 - 6
    day -= 1
    if day <= 0:
        day = get_month_days(year, month)
        month -= 1
    if month <= 0:
        month = 12
        year -= 1
    #print (year, month, day, hour)
    return year, month, day, hour
   
def check_month_day_hour (year, month, day, hour):    
    if hour < 0 or hour > 24 or hour%6!=0:
        return -1
    if day <1 or day >  get_month_days(year, month):
        return -2      
    if month < 1 or month > 12:
        return -3
    return 0

def is_leap_year (year):
    if year % 100 == 0 and year % 400 == 0 :
        return 1
    elif year % 4 == 0 :
        return 1
    else:
        return 0      

def get_month_days(year, month):
    month_days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    leap = 0
    if month == 2:
        leap = is_leap_year (year)
    return (month_days[month-1]+leap)              
        
def get_file_suffix(year, month):
    month_days = get_month_days(year, month)          
    return  f"{year:04d}{month:02d}01-{year:04d}{month:02d}{month_days:02d}"    

def get_sfc_path(era5_var, datetime):
    year   = int (datetime[ :4])
    month  = int (datetime[5:7])  
    file_suffix = get_file_suffix( int(year), int (month) )

    if   era5_var == 't2m': file_var_name = '2t'
    elif era5_var == 'u10': file_var_name = '10u' 
    elif era5_var == 'v10': file_var_name = '10v'               
    elif era5_var == 'u100':file_var_name = '100u'
    elif era5_var == 'v100':file_var_name = '100v'
    else: file_var_name =  era5_var             
        
    sfc_path = "/g/data/rt52/era5/single-levels/reanalysis/" + \
                f"{file_var_name}/{year}/{file_var_name}_era5_oper_sfc_{file_suffix}.nc"                                     
    return sfc_path                                            

def get_pl_path(era5_var, datetime):
    year   = int (datetime[ :4])
    month  = int (datetime[5:7])  
    file_suffix = get_file_suffix( int(year), int (month) )
    
    pl_path = "/g/data/rt52/era5/pressure-levels/reanalysis/" + \
                f"{era5_var}/{year}/{era5_var}_era5_oper_pl_{file_suffix}.nc"
    return pl_path
     
 