import xarray as xr
import numpy  as np
import os
from   pathlib import Path
import time
from timeit import default_timer as timer
from datetime import timedelta

import onnx
import onnxruntime as ort
import logging
import fengwu

logging.basicConfig(
    format='[%(asctime)s][%(module)s: %(filename)s: %(funcName)s: %(lineno)d]%(levelname)-4s: %(message)s',
    datefmt='%Y-%m-%d:%H:%M:%S',
    level=logging.DEBUG)
logger = logging.getLogger(__name__)

def one_data_block (datetime):

    sfc_vars = ['u10', 'v10', 't2m', 'msl']
    pl_vars  = ['z', 'q', 'u', 'v', 't']
    levels   = [50, 100, 150, 200, 250, 300, 400, 500, 600, 700, 850, 925, 1000]

    new_data = []
    
    for sfc_var in sfc_vars:
        sfc_path = fengwu.util.get_sfc_path (sfc_var, datetime) 
        sfc_data = xr.open_dataset(sfc_path)[[sfc_var]].sel(time=[datetime]).roll(longitude=int(1440/2)).load()
        new_data.append(sfc_data.to_array().squeeze().squeeze().to_numpy())
        sfc_data.close()

    for pl_var in pl_vars:
        pl_path = fengwu.util.get_pl_path (pl_var, datetime) 
        for level in levels :
            pl_data = xr.open_dataset( pl_path )[[pl_var]].sel(time=[datetime], level=[level]).roll(longitude=int(1440/2)).load()
            new_data.append(pl_data.to_array().squeeze().squeeze().to_numpy())
            pl_data.close()
    numpyArray = np.array( new_data ) 
    return numpyArray
        
    
def input_make (year, month, day, hour, save_dir_prefix):

    # Check if date and time are in the correct format
    if fengwu.util.check_month_day_hour (year, month, day, hour) !=0 : 
        exit ("Exit: Invalid data/time")
            
    # Create a folder with date time name.
    save_datetime_file = f"{year:04d}-{month:02d}-{day:02d}T{hour:02d}" 
    input_date_dir = os.path.join( save_dir_prefix, save_datetime_file )
    Path(input_date_dir).mkdir(parents=True, exist_ok=True)

    logging.info (f"FengWu: Creating input, date: {save_datetime_file} ")
    logging.info (f"FengWu: Input dir : {input_date_dir}")

    # Input 2: Second data block
    input2_datetime = fengwu.util.to_datetime(year, month, day, hour)
    logging.info (f"input2_datetime: {input2_datetime}")
    numpyArray2 = one_data_block (input2_datetime)
    input_path2 = os.path.join( input_date_dir, f"{save_datetime_file}_input2.npy" )
    logging.info (input_path2)
    np.save( input_path2, numpyArray2 )

    # Inpirst data block, six hours before
    input1_datetime = fengwu.util.to_datetime( *fengwu.util.six_hour_before_date(year, month, day, hour) )
    logging.info (f"input1_datetime: {input1_datetime}")
    numpyArray1 = one_data_block (input1_datetime)
    input_path1  = os.path.join( input_date_dir, f"{save_datetime_file}_input1.npy" )
    logging.info (input_path1)
    np.save( input_path1, numpyArray1 )

    return input_date_dir, save_datetime_file


def inference (model_dir, model_name, in_stats_dir, input_data_dir, input_datetime, output_pred_dir, steps):

    logging.info (f"FengWu: Inference, input date: {input_datetime} ")
    logging.info (f"FengWu: Inference, input dir : {input_data_dir} ")
    logging.info (f"FengWu: Inference, device    : {ort.get_device()} ")
    
    model_path = os.path.join(model_dir, model_name)
    #model_6 = onnx.load('fengwu.onnx')
    model_6 = onnx.load(model_path)
    
    # Set the behavier of onnxruntime
    options = ort.SessionOptions()
    options.enable_cpu_mem_arena=False
    options.enable_mem_pattern = False
    options.enable_mem_reuse = False
    # Increase the number for faster inference and more memory consumption
    options.intra_op_num_threads = 1
    
    # Set the behavier of cuda provider
    cuda_provider_options = {'arena_extend_strategy':'kSameAsRequested',}
    
    # Initialize onnxruntime session for Pangu-Weather Models
    #ort_session_6 = ort.InferenceSession('fengwu.onnx', sess_options=options, providers=[('CUDAExecutionProvider', cuda_provider_options)])
    ort_session_6 = ort.InferenceSession(model_path, sess_options=options, providers=[('CUDAExecutionProvider', cuda_provider_options)])
    
    #data_mean = np.load("data_mean.npy")[:, np.newaxis, np.newaxis]
    #data_std = np.load("data_std.npy")[:, np.newaxis, np.newaxis]
    data_mean = np.load( os.path.join(in_stats_dir, "data_mean.npy"))[:, np.newaxis, np.newaxis]
    data_std  = np.load( os.path.join(in_stats_dir, "data_std.npy") )[:, np.newaxis, np.newaxis]
    
    
    #input1 = np.load(os.path.join(input_data_dir, 'input1.npy')).astype(np.float32)
    #input2 = np.load(os.path.join(input_data_dir, 'input2.npy')).astype(np.float32)
    input1 = np.load(os.path.join(input_data_dir, f"{input_datetime}_input1.npy" )).astype(np.float32)
    input2 = np.load(os.path.join(input_data_dir, f"{input_datetime}_input2.npy" )).astype(np.float32)
    # input1 = np.random.rand(69, 721, 1440)
    # input2 = np.random.rand(69, 721, 1440)
    
    input1_after_norm = (input1 - data_mean) / data_std
    input2_after_norm = (input2 - data_mean) / data_std
    input = np.concatenate((input1_after_norm, input2_after_norm), axis=0)[np.newaxis, :, :, :]
    input = input.astype(np.float32)

    output_datetime = fengwu.util.six_hour_ahead ( input_datetime )
    #for i in range(56):
    start = timer()
    for i in range( int(steps) ):
        output = ort_session_6.run(None, {'input':input})[0]
        input = np.concatenate((input[:, 69:], output[:, :69]), axis=1)
        output = (output[0, :69] * data_std) + data_mean
        logging.info (f"{i:02d} Pred date: {output_datetime}   {output.shape}")
        # np.save(os.path.join(output_data_dir, f"output_{i}.npy"), output)       #保存输出
        logging.info (f"{i:02d} Pred file: {os.path.join(output_pred_dir, f'{input_datetime}_output-{i:03d}_{output_datetime}.npy')}")
        np.save(os.path.join(output_pred_dir, f"{input_datetime}_output-{i:03d}_{output_datetime}.npy"), output)     
        output_datetime = fengwu.util.six_hour_ahead ( output_datetime )
    end = timer()
    logging.info (f"FengWu: Inference time: {timedelta(seconds=end-start)}")
     
    