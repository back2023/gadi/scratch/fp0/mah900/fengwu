import sys
sys.path.insert(0, "/scratch/fp0/mah900/FengWu")
import eccodes
import os
#os.chdir('/scratch/fp0/mah900/FengWu')
import numpy as np
import fengwu
import logging
from timeit import default_timer as timer
from datetime import timedelta

MODEL_DIR   = '/g/data/wb00/admin/testing/FengWu'
MODEL_NAME  = 'fengwu_v1.onnx'
IN_STAT_DIR = '/g/data/wb00/admin/testing/FengWu/'
OUTPUT_STAT_DIR = '/g/data/wb00/admin/testing/FengWu/stat_data'  
TMP_DIR     =  os.path.join ( os.environ.get("TMPDIR", "/tmp") , "FengWu")

logging.basicConfig(
    format='[%(asctime)s][%(module)s: %(filename)s: %(funcName)s: %(lineno)d]%(levelname)-4s: %(message)s',
    datefmt='%Y-%m-%d:%H:%M:%S',
    level=logging.DEBUG)
logger = logging.getLogger(__name__)

def month_pred_stat (output_data_dir, year, month, output_stat_dir, steps):

    all_month_datetime = fengwu.util.get_all_datetimes_within_a_month (f"{year:04d}-{month:02d}-01")

    for i, datetime in enumerate(all_month_datetime):
        start = timer()
        logging.info(100*u'\u2550') 
        logging.info(f"FengWu: Input {i:02d}: {datetime}") 
        save_date_dir, save_datetime_file = fengwu.inference.input_make (int(datetime[0:4]), 
                                                                         int(datetime[5:7]), 
                                                                         int(datetime[8:10]), 
                                                                         int(datetime[11:13]), 
                                                                         output_data_dir)
        
        fengwu.inference.inference (model_dir   = MODEL_DIR,
                                    model_name  = MODEL_NAME, 
                                    in_stats_dir= IN_STAT_DIR, 
                                    input_data_dir = save_date_dir, 
                                    input_datetime = save_datetime_file, 
                                    output_pred_dir= save_date_dir, 
                                    steps = 112)

        end = timer()
        logging.info (f"{i:02d}: {timedelta(seconds=end-start)}")
        break

if __name__ == "__main__":
    
     fengwu.pred_stat.month_pred_stat(TMP_DIR, 2018, 1, OUTPUT_STAT_DIR, 112  )
        

    
    

    
