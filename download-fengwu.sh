#!/bin/bash
#PBS -S /bin/bash
#PBS -q copyq
#PBS -l ncpus=1
#PBS -l jobfs=10GB
#PBS -l storage=gdata/wb00 
# scratch/fp0+gdata/z00+gdata/fp0+gdata/dk92+gdata/in10+gdata/fs38+scratch/z00+gdata/wb00+scratch/vp91 
#PBS -l mem=20GB
#PBS -l walltime=00:30:00
#PBS -N cqp_FengWu
#PBS -P fp0

cd /g/data/wb00/admin/testing/FengWu

curl 'https://pjlab-my.sharepoint.cn/personal/chenkang_pjlab_org_cn/_layouts/15/download.aspx?SourceUrl=%2Fpersonal%2Fchenkang%5Fpjlab%5Forg%5Fcn%2FDocuments%2Ffengwu%5Fv1%2Eonnx' \
  -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7' \
  -H 'Accept-Language: en-GB,en-US;q=0.9,en;q=0.8' \
  -H 'Connection: keep-alive' \
  -H 'Cookie: FedAuth=77u/PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48U1A+VjEzLDBoLmZ8bWVtYmVyc2hpcHx1cm4lM2FzcG8lM2Fhbm9uI2Q1ZDQ3OTk3NDBhMzY2MjdhYzYyMGFmYjhhMDNmZWQyNDQ0YWZjN2YxODYzNGQyNzhhMDNjNDllYmVlYTc5MGIsMCMuZnxtZW1iZXJzaGlwfHVybiUzYXNwbyUzYWFub24jZDVkNDc5OTc0MGEzNjYyN2FjNjIwYWZiOGEwM2ZlZDI0NDRhZmM3ZjE4NjM0ZDI3OGEwM2M0OWViZWVhNzkwYiwxMzM0NjU5MjA1MDAwMDAwMDAsMCwxMzM0NjY3ODE1MDI2MTQ1NzksMC4wLjAuMCwyNTgsMGNiZTg3NWQtOGNjMy00Mzg2LTgyODMtMGQ4MGJiZjIzZTA1LCwsMmEwNWY2YTAtNzA4MS0wMDAwLTE5OWMtMWM4OTk5YzQyZmRlLDJhMDVmNmEwLTcwODEtMDAwMC0xOTljLTFjODk5OWM0MmZkZSxnalY1OVp1RGRVS3BhU2h4VlhkQUxnLDAsMCwwLCwsLDI2NTA0Njc3NDM5OTk5OTk5OTksMCwsLCwsLCwwLCw3NDEsSi1aSXFsTk15eXR5TXNmcHpyNWI4RzU1aFpJLGRLbVRIZGVaSm16V29iUTdDdEg4WGtiWW1SSElVd2tybmU2b2kzQjVEV1JkZlhvdVJOU1ZVUXRucmFDNzlxbUxuOVVDVzQ2YzNBQTJrUjg0aFIrZ3d3RmdiZVBRWnZMdVNQenNBNWdJV01lY1FFdVFnd29IU2ZZb2FyYjBxcUUra0FoZXErR1ZDK3FXdXRCVHgvTXRWWWdGVzRPaC9RWjFYUGpTaE5oU29kTjBwTjFnakVxbEt3YlZ3dDBDbm1SRzNRd3FoTnlmOHdWQzV0ZUtOS3hsVUkzSGxFV0FXcjFSSkV3akJ5QkRONGFTbEdZN2ZVby9mWGFzZnRpMS91dzZHUkUrMTc4U2pYS0ZFblkzeE9ia0VCOWorY3dsQXJ5dkJ4TjRnRXNUZ1pGRmNLMTMwc29jSDU4Yjh2U0tQYWZaSHh1QkNTWUN0LzlmajZDMFR2QjgyZz09PC9TUD4=; MicrosoftApplicationsTelemetryDeviceId=77fb1486-4add-46a4-be73-32df3ef026ef; MSFPC=GUID=9da5fb3416214417bd141516aae6aa2d&HASH=9da5&LV=202302&V=4&LU=1677470532435; ai_session=mLfrdIcPXwc/EBe0KzGLQ2|1702118158013|1702118867165' \
  -H 'DNT: 1' \
  -H 'Referer: https://pjlab-my.sharepoint.cn/personal/chenkang_pjlab_org_cn/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fchenkang%5Fpjlab%5Forg%5Fcn%2FDocuments%2Ffengwu%5Fv1%2Eonnx&parent=%2Fpersonal%2Fchenkang%5Fpjlab%5Forg%5Fcn%2FDocuments&ga=1' \
  -H 'Sec-Fetch-Dest: iframe' \
  -H 'Sec-Fetch-Mode: navigate' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-User: ?1' \
  -H 'Service-Worker-Navigation-Preload: true' \
  -H 'Upgrade-Insecure-Requests: 1' \
  -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36' \
  -H 'sec-ch-ua: "Google Chrome";v="119", "Chromium";v="119", "Not?A_Brand";v="24"' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'sec-ch-ua-platform: "macOS"' \
  --compressed  \
  --output fengwu_v1.onnx


curl 'https://pjlab-my.sharepoint.cn/personal/chenkang_pjlab_org_cn/_layouts/15/download.aspx?SourceUrl=%2Fpersonal%2Fchenkang%5Fpjlab%5Forg%5Fcn%2FDocuments%2Ffengwu%5Fv2%2Eonnx' \
  -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7' \
  -H 'Accept-Language: en-GB,en-US;q=0.9,en;q=0.8' \
  -H 'Connection: keep-alive' \
  -H 'Cookie: FedAuth=77u/PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48U1A+VjEzLDBoLmZ8bWVtYmVyc2hpcHx1cm4lM2FzcG8lM2Fhbm9uI2Q1ZDQ3OTk3NDBhMzY2MjdhYzYyMGFmYjhhMDNmZWQyNDQ0YWZjN2YxODYzNGQyNzhhMDNjNDllYmVlYTc5MGIsMCMuZnxtZW1iZXJzaGlwfHVybiUzYXNwbyUzYWFub24jZDVkNDc5OTc0MGEzNjYyN2FjNjIwYWZiOGEwM2ZlZDI0NDRhZmM3ZjE4NjM0ZDI3OGEwM2M0OWViZWVhNzkwYiwxMzM0NjU5MjA1MDAwMDAwMDAsMCwxMzM0NjY3ODE1MDI2MTQ1NzksMC4wLjAuMCwyNTgsMGNiZTg3NWQtOGNjMy00Mzg2LTgyODMtMGQ4MGJiZjIzZTA1LCwsMmEwNWY2YTAtNzA4MS0wMDAwLTE5OWMtMWM4OTk5YzQyZmRlLDJhMDVmNmEwLTcwODEtMDAwMC0xOTljLTFjODk5OWM0MmZkZSxnalY1OVp1RGRVS3BhU2h4VlhkQUxnLDAsMCwwLCwsLDI2NTA0Njc3NDM5OTk5OTk5OTksMCwsLCwsLCwwLCw3NDEsSi1aSXFsTk15eXR5TXNmcHpyNWI4RzU1aFpJLGRLbVRIZGVaSm16V29iUTdDdEg4WGtiWW1SSElVd2tybmU2b2kzQjVEV1JkZlhvdVJOU1ZVUXRucmFDNzlxbUxuOVVDVzQ2YzNBQTJrUjg0aFIrZ3d3RmdiZVBRWnZMdVNQenNBNWdJV01lY1FFdVFnd29IU2ZZb2FyYjBxcUUra0FoZXErR1ZDK3FXdXRCVHgvTXRWWWdGVzRPaC9RWjFYUGpTaE5oU29kTjBwTjFnakVxbEt3YlZ3dDBDbm1SRzNRd3FoTnlmOHdWQzV0ZUtOS3hsVUkzSGxFV0FXcjFSSkV3akJ5QkRONGFTbEdZN2ZVby9mWGFzZnRpMS91dzZHUkUrMTc4U2pYS0ZFblkzeE9ia0VCOWorY3dsQXJ5dkJ4TjRnRXNUZ1pGRmNLMTMwc29jSDU4Yjh2U0tQYWZaSHh1QkNTWUN0LzlmajZDMFR2QjgyZz09PC9TUD4=; MicrosoftApplicationsTelemetryDeviceId=77fb1486-4add-46a4-be73-32df3ef026ef; MSFPC=GUID=9da5fb3416214417bd141516aae6aa2d&HASH=9da5&LV=202302&V=4&LU=1677470532435; ai_session=/tN28pksm9GHqSfpYVqV97|1702122832299|1702122833096' \
  -H 'DNT: 1' \
  -H 'Referer: https://pjlab-my.sharepoint.cn/personal/chenkang_pjlab_org_cn/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fchenkang%5Fpjlab%5Forg%5Fcn%2FDocuments%2Ffengwu%5Fv2%2Eonnx&parent=%2Fpersonal%2Fchenkang%5Fpjlab%5Forg%5Fcn%2FDocuments&ga=1' \
  -H 'Sec-Fetch-Dest: iframe' \
  -H 'Sec-Fetch-Mode: navigate' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-User: ?1' \
  -H 'Service-Worker-Navigation-Preload: true' \
  -H 'Upgrade-Insecure-Requests: 1' \
  -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36' \
  -H 'sec-ch-ua: "Google Chrome";v="119", "Chromium";v="119", "Not?A_Brand";v="24"' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'sec-ch-ua-platform: "macOS"' \
  --compressed \
  --output fengwu_v2.onnx